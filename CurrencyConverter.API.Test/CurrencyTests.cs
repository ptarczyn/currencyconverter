﻿using NUnit.Framework;
using System;

namespace CurrencyConverter.API.Test
{
    [TestFixture]
    public class CurrencyTests
    {
        [Test]
        [TestCase("-999.09")]
        [TestCase("-999-0,89")]
        [TestCase("999xx")]
        [TestCase(",")]
        [TestCase(",67")]
        [TestCase("3,")]
        [TestCase("3,333")]
        [TestCase("3,333,")]
        [TestCase("42342342342343,333")]
        [TestCase("1 999 999 999")]
        [TestCase("-1")]
        [TestCase("-1,23")]
        public void When_InvalidOrOutOfRangeInput_Expect_CurrencyException(string input)
        {
            Func<string, string> fun = (data) => new Currency(data).WordPresentation;
            Assert.Throws<CurrencyException>(() => fun(input));
        }

        [Test]
        [TestCase("0", "zero dollars")]
        [TestCase("1", "one dollar")]
        [TestCase("25,1", "twenty-five dollars and ten cents")]
        [TestCase("0,01", "zero dollars and one cent")]
        [TestCase("45 100", "forty-five thousand one hundred dollars")]
        [TestCase("999 999 999,99", "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents")]
        public void Given_PropertInput_Expect_ProperOutput(string input, string output)
        {
            Assert.That(new Currency(input).WordPresentation, Is.EqualTo(output));
        }
    }
}
