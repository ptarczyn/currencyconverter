﻿using CurrencyConverter.API;
using System;
using System.ServiceModel;

namespace CurrencyConverter.Server
{
    public class CurrencyService : ICurrencyService
    {
        public string GetWordPresentation(string value)
        {
            try
            {
                return new Currency(value).WordPresentation;
            }
            catch (Exception e)
            {
                throw new FaultException<CurrencyServiceFault>(new CurrencyServiceFault { ErrorMessage = e.Message });
            }
        }
    }
}
