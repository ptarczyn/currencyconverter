﻿using System.Runtime.Serialization;

namespace CurrencyConverter.Server
{
    [DataContract]
    public class CurrencyServiceFault
    {
        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
