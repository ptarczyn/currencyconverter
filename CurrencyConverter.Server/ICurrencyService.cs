﻿using System.ServiceModel;

namespace CurrencyConverter.Server
{
    [ServiceContract]
    public interface ICurrencyService
    {
        [OperationContract]
        [FaultContract(typeof(CurrencyServiceFault))]
        string GetWordPresentation(string value);
    }
}
