﻿using CurrencyConverter.GUI.CurrencyServiceReference;
using Moq;
using NUnit.Framework;
using System;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;

namespace CurrencyConverter.GUI.Test
{
    [TestFixture]
    public class MainWindowViewModelTests
    {
        [Test]
        public void When_UserChangesText_Expect_CurrencyServiceIsCalled()
        {
            var service = new Mock<ICurrencyService>();
            service.Setup((s) => s.GetWordPresentationAsync(It.Is<string>(number => number == "123"))).Verifiable();

            MainWindowViewModel viewModel = new MainWindowViewModel(()=> { }, service.Object);
            viewModel.CurrencyAmount = "123";

            service.Verify();
        }

        [Test]
        public void When_UserEntersCurrencyAmonut_Expect_PropertWordPresentationIsDisplayed()
        {
            const string EXAMPLE_AMOUNT = "123";
            const string EXAMPLEWORDPRESENTATION = "one hundred twenty-three dolars";
            var service = new Mock<ICurrencyService>();
            service.Setup((s) => s.GetWordPresentationAsync(It.Is<string>(number => number == EXAMPLE_AMOUNT))).Returns(Task.FromResult(EXAMPLEWORDPRESENTATION));
            MainWindowViewModel viewModel = new MainWindowViewModel(() => { }, service.Object);
            viewModel.CurrencyAmount = EXAMPLE_AMOUNT;
            this.WaitForProperty(() => viewModel.WordPresentation);
            Assert.That(viewModel.WordPresentation, Is.EqualTo(EXAMPLEWORDPRESENTATION));
        }

        private void WaitForProperty<T>(Func<T> func) where T : class
        {
            for (int i = 0; i < 30; i++)
            {
                if (func() != null)
                    break;
                Thread.Sleep(100);
            }
        }


        [Test]
        public void When_UserEntersInvalidValue_ErrorIsDisplayed()
        {
            const string INVALID_DATA = "2,345";
            var service = new Mock<ICurrencyService>();
            service.Setup((s) => s.GetWordPresentationAsync(It.Is<string>(number => number == INVALID_DATA))).Throws(new FaultException<CurrencyServiceFault>(new CurrencyServiceFault()));
            MainWindowViewModel viewModel = new MainWindowViewModel(() => { }, service.Object);
            viewModel.CurrencyAmount = INVALID_DATA;
            this.WaitForProperty(() => viewModel.WordPresentation);
            StringAssert.StartsWith("Error", viewModel.WordPresentation);
            Assert.That(viewModel.IsError, Is.True);
        }
    }
}
