﻿using NUnit.Framework;
using System.ServiceModel;

namespace CurrencyConverter.Server.Test
{
    [TestFixture]
    public class CurrencyServiceTests
    {
        [Test]
        public void When_ValidInput_Expect_Someoutput()
        {
            var service = new CurrencyService();
            Assert.That(service.GetWordPresentation("123"), Is.Not.Empty);
        }

        [Test]
        public void When_InvalidInput_Expect_Exception()
        {
            var service = new CurrencyService();
            Assert.Throws<FaultException<CurrencyServiceFault>>(() => service.GetWordPresentation(","));
        }
    }
}
