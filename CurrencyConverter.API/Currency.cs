﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverter.API
{
    public class Currency
    {
        private static IDictionary<int, string> numerals = new Dictionary<int, string>()
        {
            [0] = "zero",
            [1] = "one",
            [2] = "two",
            [3] = "three",
            [4] = "four",
            [5] = "five",
            [6] = "six",
            [7] = "seven",
            [8] = "eight",
            [9] = "nine",
            [10] = "ten",
            [11] = "eleven",
            [12] = "twelve",
            [13] = "thirteen",
            [14] = "fourteen",
            [15] = "fifteen",
            [16] = "sixteen",
            [17] = "seventeen",
            [18] = "eightteen",
            [19] = "nineteen",
            [20] = "twenty",
            [30] = "thirty",
            [40] = "forty",
            [50] = "fifty",
            [60] = "sixty",
            [70] = "seventy",
            [80] = "eighty",
            [90] = "ninety",
            [100] = "hundred",
        };
        private const int MAX_DIGITS_OF_INTEGRAL_PART = 9;
        private const char DECIMAL_SEPARATOR = ',';
        private readonly string value;

        public Currency(string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentNullException(value);

            this.value = value;
        }

        public string WordPresentation
        {
            get
            {
                return ConvertToWordPresentation(this.value);
            }
        }

        private static int[] GetWholeNumberPart(string input, int decimalPointPos)
        {
            int[] result = new int[MAX_DIGITS_OF_INTEGRAL_PART];
            int resultIndex = MAX_DIGITS_OF_INTEGRAL_PART - 1;
            for (int i = decimalPointPos - 1; i >= 0; i--)
            {
                char digit = input[i];
                if (Char.IsDigit(digit))
                {
                    if (resultIndex < 0)
                    {
                        throw new CurrencyException($"Too many whole number digits");
                    }
                    result[resultIndex--] = (int)(digit - '0');
                }
                else if (digit != ' ')
                    throw new CurrencyException($"Invalid character '{digit}'");
            }
            if (resultIndex == MAX_DIGITS_OF_INTEGRAL_PART - 1)
                throw new CurrencyException($"Invalid whole number part");
            return result;
        }

        private static int[] GetDecimalPart(string input, int decimalPointPos)
        {
            int[] result = new int[2];
            if (decimalPointPos >= input.Length)
                return result;

            int resultIndex = 0;
            for (int i = decimalPointPos + 1; i < input.Length; i++)
            {
                char digit = input[i];
                if (Char.IsDigit(digit))
                {
                    if (resultIndex > 1)
                        throw new CurrencyException("Too many decimal digits");

                    result[resultIndex++] = (int)(digit - '0');
                }
                else throw new CurrencyException($"Invalid character '{digit}'");
            }
            if (resultIndex == 0)
                throw new CurrencyException($"Invalid decimal part");
            return result;
        }

        private static int ExtractNumberFromArray(int[] digits, int pos, int count)
        {
            int result = 0;
            int multiplier = 1;
            for (int i = pos + count - 1; i >= pos; i--)
            {
                result += digits[i] * multiplier;
                multiplier *= 10;
            }
            return result;
        }
        private static void AddPart(StringBuilder result, int value, string unit, bool forceValue, bool forceUnits, bool usePluralForm)
        {
            if (value > 0 || forceValue)
            {
                if (result.Length != 0)
                    result.Append(" ");
                result.Append(NumberToString(value));
            }
            if (value > 0 || forceUnits)
            {
                result.Append(" ");
                result.Append(unit);
                if (value != 1 && usePluralForm)
                    result.Append("s");
            }
        }

        private static string ConvertToWordPresentation(string input)
        {
            int decimalPointPos = input.IndexOf(DECIMAL_SEPARATOR);
            if (decimalPointPos < 0)
                decimalPointPos = input.Length;

            int[] integral = GetWholeNumberPart(input, decimalPointPos);

            int milions = ExtractNumberFromArray(integral, 0, 3);
            int thousands = ExtractNumberFromArray(integral, 3, 3);
            int ones = ExtractNumberFromArray(integral, 6, 3);
            int decimals = ExtractNumberFromArray(GetDecimalPart(input, decimalPointPos), 0, 2);

            StringBuilder result = new StringBuilder();
            AddPart(result, milions, "million", false, false, false);
            AddPart(result, thousands, "thousand", false, false, false);
            AddPart(result, ones, "dollar", milions == 0 && thousands == 0, true, true);
            if (decimals > 0)
                result.Append(" and");
            AddPart(result, decimals, "cent", false, false, true);
            return result.ToString();
        }

        private static string NumberToString(int value)
        {
            if (value <= 20)
                return numerals[value];
            else if (value < 100)
            {
                var tens = value / 10;
                var ones = value % 10;
                if (ones == 0)
                    return $"{numerals[tens * 10]}";
                else
                    return $"{numerals[tens * 10]}-{numerals[ones]}";
            }
            else if (value < 1000)
            {
                var hundreds = value / 100;
                var tens = value % 100;

                var hundredsString = $"{numerals[hundreds]} {numerals[100]}";
                if (tens == 0)
                    return hundredsString;
                else
                    return $"{hundredsString} {NumberToString(tens)}";
            }
            else
                throw new ArgumentOutOfRangeException(nameof(value));
        }
    }
}