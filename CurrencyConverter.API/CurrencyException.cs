﻿using System;

namespace CurrencyConverter.API
{
#pragma warning disable CA2237 // Mark ISerializable types with serializable
    public class CurrencyException : Exception
    {
        public CurrencyException(string message) : base(message)
        {
        }

        public CurrencyException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}