﻿using CurrencyConverter.GUI.CurrencyServiceReference;
using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CurrencyConverter.GUI
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly Action closeWindowAction;
        private readonly ICurrencyService currencyService;
        private string currencyAmount;
        private string wordPresentation;
        private bool error;

        public MainWindowViewModel(Action closeWindowAction, ICurrencyService currencyService)
        {
            if (closeWindowAction == null)
                throw new ArgumentNullException(nameof(closeWindowAction));

            this.CloseCommand = new DelegateCommand(OnCloseCommand);
            this.closeWindowAction = closeWindowAction;
            this.currencyService = currencyService;
        }

        public string CurrencyAmount
        {
            get => currencyAmount;
            set
            {
                currencyAmount = value;
                this.OnPropertyChanged(nameof(this.CurrencyAmount));
                this.PresentWordRepresentation();
            }
        }

        public bool IsError
        {
            get => error;
            set
            {
                error = value;
                this.OnPropertyChanged(nameof(this.IsError));
            }
        }

        public string WordPresentation
        {
            get => wordPresentation;

            set
            {
                wordPresentation = value;
                this.OnPropertyChanged(nameof(this.WordPresentation));
            }
        }

        public ICommand CloseCommand { get; }

        private void OnCloseCommand(object input)
        {
            this.closeWindowAction();
        }

        private async void PresentWordRepresentation()
        {
            string input = this.currencyAmount;
            if (string.IsNullOrWhiteSpace(input))
                this.WordPresentation = string.Empty;
            else
            {
                var response = await this.GetWordPresentationAsync(input);
                if (response.Item1)
                {
                    this.WordPresentation = response.Item2;
                    this.IsError = false;
                }
                else
                {
                    this.WordPresentation = "Error: " + response.Item2;
                    this.IsError = true;
                }
            }
        }

        private async Task<(bool, string)> GetWordPresentationAsync(string input)
        {
            try
            {
                return (true, await this.currencyService.GetWordPresentationAsync(input));
            }
            catch (FaultException<CurrencyServiceFault> e)
            {
                return (false, e.Detail.ErrorMessage);
            }
        }
    }
}