﻿using CurrencyConverter.GUI.CurrencyServiceReference;
using System.Windows;

namespace CurrencyConverter.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.DataContext = new MainWindowViewModel(this.Close, new CurrencyServiceClient("CurrencyServiceEndpoint"));
            InitializeComponent();
        }
    }
}
